﻿using System.Configuration;
using RestWorkerConsole.Utils;

namespace RestWorkerConsole.Configuration
{
    /// <summary>
    ///  Класс,  в котром хранятся строковые настройки приложения. Считываются из app.config
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// URL для загрузки  данных с внешнего ресурса
        /// </summary>
        public static string UrlToGetXml 
        {
            get { return ReadSetting("urlToGetXml"); }
        }

        /// <summary>
        /// URL для отправки данных на внешнего ресурса
        /// </summary>
        public static string UrlToSend
        {
            get { return ReadSetting("urlToSend"); }
        }

        /// <summary>
        /// URL с файлом для загрузки
        /// </summary>
        public static int OfferIdToFind
        {
            get { return int.Parse(ReadSetting("offerIdToFind")); }
        }


        /// <summary>
        /// Считывает значения по ключу из файла app.config
        /// </summary>
        /// <param name="key">Ключ, название элемента</param>
        /// <returns>Значение элемента</returns>
        private static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                return appSettings[key] ?? string.Empty;
            }
            catch (ConfigurationErrorsException e)
            {
                Logger.Log.Error("Error in AppSettings.ReadSettings\r\n", e);
            }
            return string.Empty;
        }
    }
}
