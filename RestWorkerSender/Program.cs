﻿using Newtonsoft.Json;
using RestWorkerConsole.Configuration;
using RestWorkerConsole.Helpers;
using RestWorkerConsole.Models.Xml;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestWorkerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DoMainProcessAsync();

            //Console.WriteLine("Sending data finished. Press any key to exit...");
            Console.ReadKey();
        }

        /// <summary>
        ///  Основная логика приложения в этом методе.
        /// </summary>
        /// <returns></returns>
        private static async void DoMainProcessAsync()
        {
            // get file from url and convert to string
            string contentFromUrl = await SimpleWebClient.GetFileFromUrlAcync(AppSettings.UrlToGetXml);
            Console.WriteLine("XML:\r\n" + contentFromUrl);

            //desearilize xml string to objects
            var ymlCatalog=  await XmlParser.XmlToObjectAsync<yml_catalog>(contentFromUrl);
            List<yml_catalogShopOffer> offerList = new List<yml_catalogShopOffer>(ymlCatalog.shop.offers);
            yml_catalogShopOffer offerToSend = offerList.Find(x => x.id == AppSettings.OfferIdToFind);

            // serialize objects to json
            var data = JsonConvert.SerializeObject(offerToSend); 
            Console.WriteLine("JSON:\r\n" + data);

            //send json to url
            SimpleWebClient.SendFileToUrlAsync(AppSettings.UrlToSend, data);
        }

    }
}
