﻿using RestWorkerConsole.Utils;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RestWorkerConsole.Helpers
{
   public class SimpleWebClient
    {
       public enum WebMethods
       {
           POST,
           GET
       }


       public static async Task<string> GetFileFromUrlAcync(string url)
       {
           using (var client = new WebClient())
           {
               try
               {
                   return await client.DownloadStringTaskAsync(url);
               }
               catch (Exception e)
               {
                   Logger.Log.Error("Error in SimpleWebClient.GetFileFromUrlAcync\r\n", e);
                   return null;
               }
           }
       }


       public static async void SendFileToUrlAsync(string url, string data)
       {
           using (var wc = new WebClient())
           {
               wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
              // wc.Encoding = Encoding.Default;
               wc.UploadStringAsync(new Uri(url), WebMethods.POST.ToString(), data);
           }
       }
    }
}
