﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RestWorkerConsole.Helpers
{
    /// <summary>
    /// Класс для работы с xml данными
    /// </summary>
    public static class XmlParser
    {

        /// <summary>
        /// Метод десериализует xml в класс
        /// </summary>
        /// <typeparam name="T">Тип объекта, в который будут конвертироваться данные</typeparam>
        /// <param name="xmlString">Данные типа xml в виде строки</param>
        /// <returns></returns>
        public static async Task<T> XmlToObjectAsync<T>(string xmlString)
        {
            var xs = new XmlSerializer(typeof (T));
            var memoryStream = new MemoryStream(StringToUtf8ByteArray(xmlString));
            return (T) xs.Deserialize(memoryStream);
        }

        /// <summary>
        ///   Метод конвертирует строку в UTF8 Byte массив
        /// </summary>
        /// <param name="xmlString">Данные типа xml в виде строки</param>
        /// <returns>Массив Byte[] в кодировке UTF8</returns>
        private static Byte[] StringToUtf8ByteArray(string xmlString)
        {
            var byteArray = Encoding.Default.GetBytes(xmlString);
            return byteArray;
        }             
    }
}