﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RestWorkerListener.Utils;

namespace RestWorkerListener.Helpers
{
    public class SimpleWebListener
    {
        /// <summary>
        /// Ожидает данные по указанному URL и выводит их на экран
        /// </summary>
        /// <param name="urlPrefix">Прослушиваемый URL</param>
        /// <returns></returns>
        public static async void ClientRequestAwait(string urlPrefix)
        {
            // Create a listener.
            HttpListener listener = new HttpListener();
            try
            {
                // URI prefix are required,
                // for example "http://contoso.com:8080/index/".
                if (urlPrefix == null)
                    throw new ArgumentException("URL for listen is empty!");

                // Add the prefix.    
                listener.Prefixes.Add(urlPrefix);
                listener.Start();
                Console.WriteLine("Listening...");
                // Note: The GetContext method blocks while waiting for a request. 
                Console.WriteLine("Waiting for client.. ");
                var context = await listener.GetContextAsync();
                Console.WriteLine("Client connected!");

                GetRequestFromClientAsync(context);
            }
            catch (Exception e)
            {
                Console.WriteLine("Listen service error...");
                Logger.Log.Error("Error in SimpleWebListener.ClientRequestAwait\r\n", e);
            }
            listener.Stop();
        }

        /// <summary>
        /// Вывод данных, полученных в запросе
        /// </summary>
        /// <param name="context">Листенер, содержащий данные для вывода</param>
        private static async void GetRequestFromClientAsync(HttpListenerContext context)
        {
            try
            {
                var request = context.Request;
                string response;
                using (var reader = new StreamReader(request.InputStream, Encoding.Default))
                {
                    response = await reader.ReadToEndAsync();
                }
                Console.WriteLine("Content:\r\n {0}", response);
            }
            catch (WebException e)
            {
                Console.WriteLine("Error reading content...");
                Logger.Log.Error("Error in SimpleWebListener.GetRequestFromClientAsync\r\n", e);
            }
        }
    }
}
