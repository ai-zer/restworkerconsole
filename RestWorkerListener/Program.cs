﻿using RestWorkerListener.Helpers;
using System;
using RestWorkerListener.Configuration;

namespace RestWorkerListener
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            SimpleWebListener.ClientRequestAwait(AppSettings.ListenUrl);

            //Console.WriteLine("Recieving data finished. Press any key to exit...");
            Console.ReadKey();
        }
    }
}