﻿using System.Configuration;
using RestWorkerListener.Utils;

namespace RestWorkerListener.Configuration
{
    /// <summary>
    ///  Класс,  в котром хранятся строковые настройки приложения. Считываются из app.config
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// URL, который будет прослушивать приложение
        /// </summary>
        public static string ListenUrl
        {
            get { return ReadSetting("listenUrl"); }
        }


        /// <summary>
        /// Считывает значения по ключу из файла app.config
        /// </summary>
        /// <param name="key">Ключ, название элемента</param>
        /// <returns>Значение элемента</returns>
        private static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                return appSettings[key] ?? string.Empty;
            }
            catch (ConfigurationErrorsException e)
            {
                Logger.Log.Error("Error in AppSettings.ReadSettings\r\n", e);
            }
            return string.Empty;
        }
    }
}
